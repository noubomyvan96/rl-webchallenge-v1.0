import React from 'react'
import Link from 'next/link';



// Function to fetch products from the API
async function getProducts(){

    const res = await fetch("https://api.restful-api.dev/objects" ,{

        // Configuration Object for fetch options
        next : {
            revalidate : 0 // use 0 to opt out of using cache
        }
    })

    return res.json()
}


// Default function component for ProductList
export default async function ProductList() {


// Fetch products asynchronously before rendering
const products = await getProducts();
  return (
    <>
     {/* Map through the products array and render each product as a card */}
    {products.map((product)=>(
        <div key={product.id} className='card m-5'>
        <Link href={`/products/${product.id}`}> 
            <h3>{product.name}</h3>   
        </Link>  
        </div>
        
    ))}
    {/* Conditional rendering when there are no products */}
    {products.length === 0 && (
        <p className='text-center'> There are no products</p>
    )}
    </>
      )
}
