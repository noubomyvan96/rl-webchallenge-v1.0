"use client"

import { useRouter } from "next/navigation"
import { useState } from "react"




export default  function CreateForm() {

    // useRouter hook from Next.js for client-side navigation
    const router = useRouter()


    // State variables to hold form input values
    const [name, setName] = useState('');
    const [year, setYear] = useState('');
    const [price, setPrice] = useState('');
    const [cpuModel, setCpuModel] = useState('');
    const [hardDiskSize, setHardDiskSize] = useState('');

    // State variable to manage loading state during form submission
    const [isLoading, setIsLoading] = useState(false)


    // Handle form submission
    const handleSubmit = async (event) => {
        event.preventDefault();
        setIsLoading(true);


        // Create a new object with form data
        const newObject = {
          name,
          data: {
            year,
            price,
            "CPU model": cpuModel,
            "Hard disk size": hardDiskSize,
          },
        };
try{
        // Send a POST request to the API to add the new object
        const res = await fetch('https://api.restful-api.dev/objects', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(newObject)
          })

// If object creation is successful (status code 201), refresh the page and navigate to the products page
          if (res.status === 201) {
            router.refresh() // Refresh the data to reflect the new object
            router.push('/products')// Navigate to the products page
          }else{
            // Handle other HTTP statuses (e.g., display an error message)
        console.error('Failed to add product:', res.status);
        // Optionally handle error cases, display an alert, or update UI state

          }
        }catch(error){
            console.error('Error adding product:', error);
      // Handle fetch errors, display an alert, or update UI state accordingly
        }finally {
            setIsLoading(false); // Reset loading state regardless of success or failure
          }
      };

  return (
    <form onSubmit={handleSubmit}>
    <div>
      <label>Name:</label>
      <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
    </div>
    <div>
      <label>Year:</label>
      <input type="text" value={year} onChange={(e) => setYear(e.target.value)} />
    </div>
    <div>
      <label>Price:</label>
      <input type="text" value={price} onChange={(e) => setPrice(e.target.value)} />
    </div>
    <div>
      <label>CPU Model:</label>
      <input type="text" value={cpuModel} onChange={(e) => setCpuModel(e.target.value)} />
    </div>
    <div>
      <label>Hard Disk Size:</label>
      <input type="text" value={hardDiskSize} onChange={(e) => setHardDiskSize(e.target.value)} />
    </div>
    <button className="btn-primay" disabled={isLoading} type="submit">
    {isLoading && <span>Adding...</span>}
    {!isLoading && <span>Add Product</span>}
    </button>
  </form>
    
  );
};
