import CreateForm from "./createForm"

export default  function createProduct() {
  return (
    <main>
        <h2 className="text-primary text-center "> Add New Product</h2>
        <CreateForm />
    </main>
  )
}
