import React from 'react'
 
import ProductList from './ProductList'

export default function Products() {
  return (
    <main>
       <nav>
        <div>
            <h2>Products</h2>
            <p><small> Currently open products</small></p>
        </div>
       </nav>
       <ProductList/>
    </main>
  )
}
