import dynamic from 'next/dynamic';
import { notFound } from 'next/navigation';
import React from 'react'

export const dynamicParams = true ;


export async function generateStaticParams() {
    const res = await fetch('https://api.restful-api.dev/objects')
  
    const products = await res.json()
   
    return products.map((product) => ({
      id: product.id
    }))
  }


async function getProduct(id){

    const res = await fetch("https://api.restful-api.dev/objects/" +id ,{
        next : {
            revalidate : 0 // use 0 to opt out of using cache
        }
    })

    if (!res.ok){
        notFound();
    }

    return res.json()
}

export default async function ProductDetails({params}) {

const product = await getProduct(params.id);

      

  return (
    <main>
        <nav>
            <h2> Product Details</h2>

        </nav>
        <div className='card'>
            <h3>{product.name}</h3>
        </div>
    </main>
  )
}
