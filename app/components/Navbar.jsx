import React from 'react'
import Link from 'next/link'

export default function Navbar() {
  return (
    <nav>
    <h1> Test navbar</h1>
    <Link href="/"> Dashboard</Link>
    <Link href="/products">Products</Link>
    <Link href="products/create"> Create Product</Link>
  </nav>
  )
}
